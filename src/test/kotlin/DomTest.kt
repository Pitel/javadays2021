import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlinx.browser.document

class DomTest : StringSpec({
    beforeTest {
        document.clear()
        document.write("""<body><p id="add"></p><p id="datefns"></p></body>""")
        main()
    }

    "Add works" {
        document.getElementById("add")?.textContent shouldBe "5"
    }

    "date-fns works" {
        document.getElementById("datefns")?.textContent shouldBe "10. 11. 2021"
    }
})