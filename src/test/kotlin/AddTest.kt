import io.kotest.core.spec.style.StringSpec
import io.kotest.property.forAll

class AddTest: StringSpec({
    "Adding works as expected" {
        forAll<Int, Int> { a, b ->
            add(a, b) == a + b
        }
    }
})