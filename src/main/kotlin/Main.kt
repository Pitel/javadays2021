import kotlinx.browser.document
import kotlin.js.Date

fun main() {
    document.getElementById("add")?.textContent = "${add(2, 3)}"
    document.getElementById("datefns")?.textContent = format(Date(2021, 10, 10), "d. M. yyy")
}

fun add(a: Int, b: Int) = a + b