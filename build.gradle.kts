import org.jetbrains.kotlin.gradle.dsl.KotlinCompile
import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootExtension
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    val kotlinVersion = "1.5.31"
    kotlin("js") version kotlinVersion
    id("io.kotest.multiplatform") version "5.0.0.5"
}

repositories {
    mavenCentral()
}

kotlin {
    js(IR) {
        browser()
        binaries.executable()
    }
}

tasks.withType<KotlinCompile<*>> {
    kotlinOptions {
        freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    }
}

tasks.withType<Kotlin2JsCompile> {
    if (!name.contains("Test")) {
        kotlinOptions {
            freeCompilerArgs += "-Xir-property-lazy-initialization"
        }
    }
}

// https://youtrack.jetbrains.com/issue/KT-49124
rootProject.extensions.configure<NodeJsRootExtension> {
    versions.webpackCli.version = "4.9.0"
}

dependencies {
    implementation(npm("date-fns", "2.25.0"))

    val kotest = "5.0.0.M3"
    testImplementation("io.kotest:kotest-framework-engine:$kotest")
    testImplementation("io.kotest:kotest-assertions-core:$kotest")
    testImplementation("io.kotest:kotest-property:$kotest")
}